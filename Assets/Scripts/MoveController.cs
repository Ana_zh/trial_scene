﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour {
    private float move, rotate;
    private Rigidbody rigidbody;
    public float move_speed, rotate_speed;
	void Start () {
        rigidbody = GetComponent<Rigidbody>();
	}

    private void FixedUpdate()
    {
        Move();
        Turn();
    }
    private void Move()
    {
        Vector3 movement = transform.forward * move * move_speed * Time.deltaTime;
        rigidbody.MovePosition(rigidbody.position + movement);
    }
    private void Turn()
    {
        float turn = rotate * rotate_speed * Time.deltaTime;
        Quaternion turn_rot = Quaternion.Euler(0f, turn, 0f);
        rigidbody.MoveRotation(rigidbody.rotation * turn_rot);
    }

    void Update () {
        move = Input.GetAxis("Vertical");
        rotate = Input.GetAxis("Horizontal");
    }
}
